const colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

let container = document.getElementById('colorContainer');

let colorOptions = () => {
  colorList.forEach((color, index) => {
    let classButton = ' color-button ' + color;
    if (index === 0) {
      classButton = ' active ';
    }
  });
};

colorOptions();

let colorSelect = (color) => {
  document.getElementById('house').classList.toggle(color);
};
