const colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

var stock = colorList[0];

let colorOptions = () => {
  var contentHTML = '';
  for (let i = 0; i < colorList.length; i++) {
    var contentButton = `<button onclick="colorSelect('${colorList[i]}')" class="color-button ${colorList[i]}"></button>`;
    contentHTML += contentButton;
  }
  document.getElementById('colorContainer').innerHTML = contentHTML;
};

colorOptions();

let colorSelect = (color) => {
  deleteActive();

  document.getElementById('house').classList.add(color);
  document.getElementById(color).classList.add('active');
};

let deleteActive = () => {
  document.getElementById(stock).classList.remove('active');
  document.getElementById('house').classList.remove('stock');
};
