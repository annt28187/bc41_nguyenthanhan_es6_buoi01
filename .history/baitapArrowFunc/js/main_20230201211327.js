const colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

let container = document.getElementById('colorContainer');

let colorOptions = () => {
  colorList.forEach((color, index) => {
    let classButton = ' color-button ' + color;
    if (index === 0) {
      classButton += ' active ';
    }
    container.innerHTML += `<button class="${classButton}"></button>`;
  });
};

colorOptions();

let colorPicker = document.getElementsByClassName('color-button'),
  house = document.getElementById('house');

let colorSelect = (color) => {
  document.getElementById('house').classList.toggle(color);
};
