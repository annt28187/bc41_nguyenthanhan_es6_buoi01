const colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

let colorOptions = () => {
  var contentHTML = '';
  for (let i = 0; i < colorList.length; i++) {
    var contentButton = `<button onclick="colorSelect('${colorList[i]}')" class="color-button ${colorList[i]}"></button>`;
    contentHTML += contentButton;
  }
  document.getElementById('colorContainer').innerHTML = contentHTML;
};

let colorSelect = (color) => {
  document.getElementById('house').classList.toggle(color);
};
