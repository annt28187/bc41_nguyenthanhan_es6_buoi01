const colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

let container = document.getElementById('colorContainer');

let colorOptions = () => {
  colorList.forEach((color, index) => {
    let classButton = ' color-button ' + color;
    if (index === 0) {
      classButton += ' active ';
    }
    container.innerHTML += `<button class="${classButton}"></button>`;
  });
};

colorOptions();

let colorPicker = document.getElementsByClassName('color-button'),
  house = document.getElementById('house');

for (let i =0; i < colorPicker.length;i++) colorPicker[i].addEventListener("click",function(){

});

changeColor = ((i,e){
  for(let i = 0; i < colorPicker.length;i++) changeColor[i].classList.remove("active");
  changeColor[e].classList.add("active"), house.className = "house "+i;
})
