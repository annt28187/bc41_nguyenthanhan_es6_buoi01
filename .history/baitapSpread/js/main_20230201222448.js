const heading = document.querySelector('.heading');
(jumpText = (e) => {
  return [...e].map((e) => `<span>${e}</span>`).join('');
}),
  (heading.innerHTML = jumpText(heading.innerText));
