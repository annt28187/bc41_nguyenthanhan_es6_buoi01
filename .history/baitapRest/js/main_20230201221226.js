const tinhDTB = (...e) => {
  let sum = 0;
  e.forEach((item) => {
    sum += parseFloat(item);
  });
  return (sum / e.length).toFixed(2);
};

let calKhoi1 = () => {
  let inpToanEl = document.getElementById('inpToan').value,
    inpLyEl = document.getElementById('inpLy').value,
    inpHoaEl = document.getElementById('inpHoa').value;
  document.getElementById('tbKhoi1').innerHTML = tinhDTB(inpToanEl, inpLyEl, inpHoaEl);
};
